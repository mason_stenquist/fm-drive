export const state = () => ({
    oneDrive:{
        authUri: 'https://login.microsoftonline.com/common/oauth2/v2.0/authorize',
        tokenUri: 'https://login.microsoftonline.com/common/oauth2/v2.0/token',
        redirectUri: 'http://localhost:3000/callback',
        clientID: '7c999c9c-060e-4318-802a-89adccb324f1',
        clientSecret: 'KwD*+.c1IWp_4cyoXAN5MwI6Ff8J*l0k',
        scope: 'files.readwrite.all offline_access',
        responseType: 'code',
        grantType: 'authorization_code' 
    } ,
    oneDriveTokens:{},
    currentDirectory: {},
    directory: {}
})


export const actions = {
    nuxtServerInit ({ commit }, { req }) {
      if (req.session.oneDriveTokens) {
        commit('SET_ONE_DRIVE_TOKENS', req.session.oneDriveTokens)
      }
    }
}

export const mutations = {
    SET_ONE_DRIVE_TOKENS (state, payload) {
      state.oneDriveTokens = payload
    },
    SET_DIRECTORY (state, payload) {
      let {path} = payload
      if( !state.directory[path]){
        state.directory[path] = payload
      }
      state.currentDirectory = payload  
    }
  }
const express = require('express')
const session = require('express-session')
const axios = require('axios')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
const app = express()

//setup session
app.use(session({
  secret: 'filemaker is the best',
  resave: false,
  saveUninitialized: false,
  cookie: {}
}))

// Import and Set Nuxt.js options
const config = require('../nuxt.config.js')
config.dev = process.env.NODE_ENV !== 'production'

async function start () {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  } else {
    await nuxt.ready()
  }

  app.get('/api/token', function(req, res){
    let {tokenUri, clientID, redirectUri, clientSecret, grantType} = config.oneDrive
    let encodeClient = encodeURIComponent(clientID)
    let encodeSecret = encodeURIComponent(clientSecret)
    let encodeRedirect = encodeURIComponent(redirectUri)
    let code = req.query.code
    let data = `client_id=${encodeClient}&client_secret=${encodeSecret}&code=${code}&redirect_uri=${encodeRedirect}&grant_type=${grantType}`
    console.log(data)
    axios.post(tokenUri, data, {
      
    })
      .then((response)=>{
        console.log(response.data)
        req.session.oneDriveTokens = response.data
        res.send(response.data)
      })
      .catch((e)=>{
        console.log(e)
        console.log("ERROR")
      })
  })

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()
